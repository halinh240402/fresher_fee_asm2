function isNullOrEmpty(str) {
    return !str || str.trim() === '';
}
function checkRegisterInfo(){
    let userRegister = getUserRegisterInfo();
    let username = userRegister.username;
    let email = userRegister.email;
    let password = userRegister.password;
    let repassword = userRegister.repassword;
    debugger;
    if(isNullOrEmpty(username) || isNullOrEmpty(email) || isNullOrEmpty(password) || isNullOrEmpty(repassword)){
        displayError("Enter all fields");
        return false;
    }else{
        if (email.length < 5 || email.length > 50) {
            displayError("Email length must be between 5 and 50 characters");
            return false;
        }
        if(password.length < 8 || password.length > 30){
            displayError("Password length must be between 8 and 30 characters");
            return false;
        }
        if(password != repassword){
            displayError("Password and Re password must be the same");
            return false;
        }
    }
    return true;
}

function getUserRegisterInfo(event){
    let username = document.getElementById('username').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    let repassword = document.getElementById('repassword').value;
    let user = {
        username: username,
        email: email,
        password: password,
        repassword: repassword
    };
    return user;
}
function getUserLoginInfo(){
    event.preventDefault();
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const users = getUsersFromLocalStorage();
    let currentUser = null;
    let currentIndex = 0;
    for(let i = 0; i < users.length; i++){
        if(users[i].email == email && users[i].password == password){
            currentUser = JSON.stringify(users[i]);
            currentIndex = i;
            break;
        }
    }
    //login
    if (currentUser != null) {
        localStorage.setItem('currentUser', currentUser);
        localStorage.setItem('currentIndex', currentIndex);
        window.location.href = '../html/View-content.html';
    } else {
        displayError('Invalid username or password.');
    }
}

function register(){
    event.preventDefault();
    const userRegister = getUserRegisterInfo();
    if(checkRegisterInfo() == true){
        if (checkEmailDuplicate(userRegister.email)) {
            displayError('Email is already in use.');
        } else {
            const user = {
                username: userRegister.username,
                password: userRegister.password,
                email: userRegister.email,
            };
            hideError();
            addUserToLocalStorage(user);
            alert('User registered successfully!');
            window.location.href = '../html/Login.html';
        }
    }
}

function displayError(message) {
    const errorDiv = document.getElementById('error');
    errorDiv.innerHTML = message;
    errorDiv.style.display = 'block';
}

function hideError() {
    const errorDiv = document.getElementById('error');
    errorDiv.style.display = 'none';
    console.log("Success");
}

//get users array from local storage
function getUsersFromLocalStorage() {
    const usersString = localStorage.getItem('users');
    return usersString ? JSON.parse(usersString) : [];
}

// add user to local storage
function addUserToLocalStorage(user) {
    const users = getUsersFromLocalStorage();
    users.push(user);
    localStorage.setItem('users', JSON.stringify(users));
}

function checkEmailDuplicate(email) {
    const users = getUsersFromLocalStorage();
    return users.some(user => user.email === email);
}

// check and add content
function checkContentInfo(){
    event.preventDefault();
    let _title = document.getElementById('title').value;
    let _content = document.getElementById('content').value;
    let _brief = document.getElementById('brief').value;
    if(!isNullOrEmpty(_title) && !isNullOrEmpty(_content) || !isNullOrEmpty(_brief)){
        let content = {
            title: _title,
            brief: _brief,
            content: _content
        };
        addContentToLocalStorage(content);
        alert('Content added successfully!');
    }
}
function getContentsFromLocalStorage() {
    const contentsString = localStorage.getItem('contents');
    return contentsString ? JSON.parse(contentsString) : [];
}

// add content to local storage
function addContentToLocalStorage(content) {
    const contents = getContentsFromLocalStorage();
    contents.push(content);
    localStorage.setItem('contents', JSON.stringify(contents));
}

// edit user
function editCurrentUser(){
    let listUsers = getUsersFromLocalStorage();
    let currentIndex = localStorage.getItem('currentIndex');
    let currentUser = localStorage.getItem('currentUser');
    currentUser = JSON.parse(currentUser);

    //get new info
    let newFirst = document.getElementById('firstname').value;
    let newLast = document.getElementById('lastname').value;
    let phone = document.getElementById('phone').value;
    let description = document.getElementById('description').value;
    //update
    currentUser.firstname = newFirst;
    currentUser.lastname = newLast;
    currentUser.phone = phone;
    currentUser.description = description;

    listUsers[currentIndex] = currentUser;
    localStorage.setItem('users', JSON.stringify(listUsers));
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    alert('Edit successfully!');
}
//logout
function logout(){
    console.log("Logout clicked");
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentIndex');
    window.location.href = '../html/Login.html';
}